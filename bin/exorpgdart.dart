import 'dart:math';
import 'package:exorpgdart/models/armes.dart';
import 'package:exorpgdart/models/personnage.dart';
import 'package:exorpgdart/service/pokeapi.dart';

void main(List<String> arguments) async {
  Armes Zangetsu = Armes(nom: "Zangetsu", armeAttq: 10);
  Armes Parashu = Armes(nom: "Parashu", armeAttq: 10);
  // Personnage personnage_one = Personnage(
  //     nom: 'Steeven',
  //     PdvMax: 100,
  //     Pdv: 100,
  //     PmMax: 10,
  //     Pm: 10,
  //     num: 1,
  //     compteurTour: 0,
  //     arme: Zangetsu);
  // Personnage personnage_two = Personnage(
  //     nom: 'Maxime',
  //     PdvMax: 100,
  //     Pdv: 100,
  //     PmMax: 10,
  //     Pm: 10,
  //     num: 2,
  //     compteurTour: 0,
  //     arme: Parashu);
  Pokeapi pokeapi = Pokeapi();
  Personnage personnage_one = await pokeapi.getPokemon(id: 5);
  Personnage personnage_two = await pokeapi.getPokemon(id: 69);
  var boolValue = Random().nextBool();

  while ((personnage_one.Pdv > 0) && (personnage_two.Pdv > 0)) {
    if (boolValue) {
      tour(attq: personnage_two, def: personnage_one);
      if (personnage_one.Pdv > 0) {
        tour(attq: personnage_one, def: personnage_two);
      }
    } else {
      tour(attq: personnage_one, def: personnage_two);
      if (personnage_two.Pdv > 0) {
        tour(attq: personnage_two, def: personnage_one);
      }
    }
  }
  if (personnage_two.Pdv <= 0) {
    print('${personnage_two.nom} est KO ${personnage_one.nom} gagne 152xp');
  } else if (personnage_one.Pdv <= 0) {
    print('${personnage_one.nom} est KO ${personnage_two.nom} gagne 152xp');
  }
}

void tour({
  required Personnage attq,
  required Personnage def,
}) {
  int attArme = attq.arme.armeAttq;
  int valeurAtt = (attq.puiAtt());
  num attArmee = valeurAtt += attArme;
  int valeurAttSp = attq.spAtt();
  attq.tourPlus();

  if (attq.healReady()) {
    // attq.heal();
    print(
        "${attq.nom} c'est heal de ${attq.PdvMax * 0.3} il a maintenant ${attq.Pdv} Pdv");
  } else if (attq.isReady()) {
    if (attq.Pm <= 8) {
      attq.Pm += 2;
    }
    def.Pdv -= valeurAttSp;

    print(
        "${def.nom} as subit $valeurAttSp de degats de la pars de ${attq.nom} il lui reste ${def.Pdv} Pdv.");
  } else {
    if (attq.Pm <= 8) {
      attq.Pm += 2;
    }
    // def.Pdv -= attArmee;
    print(
        "${def.nom} as subit $valeurAtt de degats de la pars de ${attq.nom} il lui reste ${def.Pdv} Pdv.");
  }

  // print('$tourSp');
  // print('$valeurTour');
  print(attq.Pm);
  print(attq.type);
}
