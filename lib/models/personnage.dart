import 'dart:math';
import 'package:exorpgdart/models/types.dart';

import 'armes.dart';

class Personnage {
  final String nom;
  double Pdv;
  int Pm;
  int num;
  int compteurTour;
  final double PdvMax;
  final int PmMax;
  Armes arme;
  Types type;

  Personnage({
    required this.nom,
    this.Pdv = 0,
    this.Pm = 0,
    required this.num,
    required this.compteurTour,
    required this.PdvMax,
    required this.PmMax,
    required this.arme,
    required this.type,
  }) {
    Pm = PmMax;
    Pdv = PdvMax;
  }
  Personnage.fromJson({required Map<String, dynamic> json})
      : nom = json["name"],
        Pdv = json["stats"]["HP"].toDouble(),
        Pm = 20,
        num = 1,
        compteurTour = 0,
        PdvMax = json["stats"]["HP"].toDouble(),
        PmMax = 20,
        arme = Armes(nom: 'Beretta', armeAttq: 9),
        type = json["apiResistances"][0];

  int puiAtt() {
    Random randomAtt = Random();
    int randomNumber = randomAtt.nextInt(10);
    return randomNumber + 1;
  }

  int spAtt() {
    return (puiAtt() + puiAtt());
  }

  void tourPlus() {
    compteurTour += 1;
  }

  bool isReady() {
    return compteurTour % 5 == 0;
  }

  bool healReady() {
    return Pdv <= (PdvMax * 0.3) && Pm == PmMax;
  }

  void heal() {
    Pdv += (PdvMax * 0.3);
    Pm = 0;
  }
}
// faire une map clee type normal et value dmg multi comme ca on sais le name on a juste  amétre cochet de ladv pour le touver
// cle string double 
