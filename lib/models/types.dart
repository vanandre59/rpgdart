class Types {
  final String nom;
  double DmgMulti;

  Types({
    required this.nom,
    required this.DmgMulti,
  });
  Types.fromJson({required Map<double, dynamic> json})
      : nom = json["apiResistances"]["name"],
        DmgMulti = json["apiResistances"]["damage_relation"];
}
