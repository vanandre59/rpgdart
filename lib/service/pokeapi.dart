import 'dart:io';

import 'package:dio/dio.dart';
import 'package:exorpgdart/models/personnage.dart';

class Pokeapi {
  Dio dio = Dio();
  static const String _url = "http://pokebuildapi.fr/api/v1/pokemon/";
  Future<Personnage> getPokemon({required int id}) async {
    final resultat = await dio.get("$_url$id");
    // print(resultat.data);
    // print(resultat.data["stats"]["HP"]);
    Personnage pokemon = Personnage.fromJson(json: resultat.data);
    // print(pokemon.nom);
    return pokemon;
  }
}
